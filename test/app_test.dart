import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/app.dart';
import 'package:sample_app/timer/views/timer_page.dart';

void main() {
  group('App', () {
    testWidgets('renders TimerPage', (widgetTester) async {
      await widgetTester.pumpWidget(const App());
      expect(find.byType(TimerScreen), findsOneWidget);
    });
  });
}
