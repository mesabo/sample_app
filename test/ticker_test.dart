import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/ticker.dart';

void main() {
  group('Ticker', () {
    const ticker = Ticker();
    test("Emit 5 ticks from 4 to 0 every second", () {
      expectLater(ticker.tick(ticks: 5), emitsInOrder(<int>[4, 3, 2, 1, 0]));
    });
  });
}
