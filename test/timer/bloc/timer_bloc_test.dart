import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sample_app/ticker.dart';
import 'package:sample_app/timer/bloc/timer_bloc.dart';

class MockTicker extends Mock implements Ticker {}

void main() {
  group("Ticker", () {
    late Ticker ticker;

    setUp(() async {
      ticker = MockTicker();
      when(() => ticker.tick(ticks: 5)).thenAnswer(
          (invocation) => Stream<int>.fromIterable([5, 4, 3, 2, 1]));
    });

    test("Initial State is TimerInitial(60) ",
        () => expect(TimerBloc(ticker: ticker).state, const TimerInitial(60)));

    blocTest<TimerBloc, TimerState>(
      "emeits TickerRunInProgress(5) when ticker is paused at 5",
      build: () => TimerBloc(ticker: ticker),
      act: (bloc) => bloc.add(const TimerStarted(duration: 5)),
      expect: () => [
        const TimerRunInProgress(5),
        const TimerRunInProgress(4),
        const TimerRunInProgress(3),
        const TimerRunInProgress(2),
        const TimerRunInProgress(1),
      ],
      verify: (bloc) => verify(() => ticker.tick(ticks: 5)).called(1),
    );

    blocTest<TimerBloc, TimerState>(
      "emeits TickerRunPause(2) when ticker is paused at 2",
      build: () => TimerBloc(ticker: ticker),
      seed: () => const TimerRunInProgress(2),
      act: (bloc) => bloc.add(const TimerPaused()),
      expect: () => [const TimerRunPause(2)],
    );
  });
}
