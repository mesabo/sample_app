import 'package:flutter_test/flutter_test.dart';
import 'package:sample_app/timer/bloc/timer_bloc.dart';

void main() {
  group("TimerState", () {
    group('TimerInital', () {
      test('Support Value Comparison',
          () => expect(const TimerInitial(60), const TimerInitial(60)));
    });
    group('TimerRunPause', () {
      test('Support Value Comparison',
          () => expect(const TimerRunPause(60), const TimerRunPause(60)));
    });

    group('TimerResumed', () {
      test('Support Value Comparison',
          () => expect(const TimerResumed(), const TimerResumed()));
    });
    group('TimerRunInProgress', () {
      test(
          'Support Value Comparison',
          () => expect(
              const TimerRunInProgress(60), const TimerRunInProgress(60)));
    });
    group('TimerRunComplete', () {
      test('Support Value Comparison',
          () => expect(const TimerRunComplete(), const TimerRunComplete()));
    });
  });
}
