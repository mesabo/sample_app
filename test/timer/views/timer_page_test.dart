import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:sample_app/timer/bloc/timer_bloc.dart';
import 'package:sample_app/timer/views/timer_page.dart';

class MockTimerBloc extends MockBloc<TimerEvent, TimerState>
    implements TimerBloc {}

class FakeEventTime extends Fake implements TimerEvent {}

extension on WidgetTester {
  Future<void> pumpTimerView(TimerBloc timerBloc) {
    return pumpWidget(MaterialApp(
      home: BlocProvider.value(value: timerBloc, child: const TimerScreen()),
    ));
  }
}

void main() {
  late TimerBloc timerBloc;
  setUpAll(() => registerFallbackValue(FakeEventTime()));

  setUp(() => timerBloc = MockTimerBloc());

  // Call after all test are run
  tearDown(() => reset(timerBloc));

  group('TimerPage', () {
    testWidgets('Renders TimerView', (widgetTester) async {
      await widgetTester.pumpWidget(const MaterialApp(home: TimerScreen()));
      expect(find.byType(TimerScreen), findsOneWidget);
    });
  });

  group('TimerScreen', () {
    testWidgets('renders initial Timer Screen', (widgetTester) async {
      when(() => timerBloc.state).thenReturn(const TimerInitial(60));
      await widgetTester.pumpTimerView(timerBloc);
      expect(find.text('01:00'), findsOneWidget);
      expect(find.byIcon(Icons.play_arrow), findsOneWidget);
    });

    // testWidgets('renders pause and reset button when timer is in progress',
    //     (widgetTester) async {
    //   when(() => timerBloc.state).thenReturn(const TimerRunInProgress(59));
    //   await widgetTester.pumpTimerView(timerBloc);
    //   expect(find.text('00:59'), findsOneWidget);
    //   expect(find.byIcon(Icons.pause), findsOneWidget);
    //   expect(find.byIcon(Icons.replay), findsOneWidget);
    // });

    // testWidgets('Renders play and reset button when timer is paused',
    //     (widgetTester) async {
    //   when(() => timerBloc.state).thenReturn(const TimerRunPause(15));
    //   await widgetTester.pumpTimerView(timerBloc);
    //   expect(find.text('00:15'), findsOneWidget);
    //   expect(find.byIcon(Icons.play_arrow), findsOneWidget);
    //   expect(find.byIcon(Icons.replay), findsOneWidget);
    // });

    // testWidgets('Renders play button when timer is completed',
    //     (widgetTester) async {
    //   when(() => timerBloc.state).thenReturn(const TimerRunComplete());
    //   await widgetTester.pumpTimerView(timerBloc);
    //   expect(find.text('00:00'), findsOneWidget);
    //   expect(find.byIcon(Icons.replay), findsOneWidget);
    // });

    // testWidgets('Renders initial TimerView', (widgetTester) async {
    //   when(() => timerBloc.state).thenReturn(const TimerInitial(30));
    //   await widgetTester.pumpTimerView(timerBloc);
    //   await widgetTester.tap(find.byIcon(Icons.play_arrow));
    //   verify(
    //     () => timerBloc.add(any(
    //         that: isA<TimerStarted>()
    //             .having((p0) => p0.duration, "duration", 30))),
    //   ).called(1);
    // });
  });
}
