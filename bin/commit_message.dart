import 'dart:io';

dynamic main() {
  final rootDir = Directory.current;
  final commitFile = File("${rootDir.path}/.git/COMMIT_EDITMSG");
  final commitMessage = commitFile.readAsStringSync();

  final regExp = RegExp(
    r'(bugfix|feature|hotfix|none|chore|refactor|doc|style|test)(\(\w+\):\s?)(\[\w+-\d+])(.+)',
  );

  final valid = regExp.hasMatch(commitMessage);
  if (!valid) exitCode = 1;
}
