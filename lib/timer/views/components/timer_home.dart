import 'package:flutter/material.dart';

import '../widgets/timer_action_burron.dart';
import '../widgets/timer_text_view.dart';


class TimerHome extends StatelessWidget {
  const TimerHome({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const [
            TimerTextView(),
            TimerActionButton(),
          ],
        ),
      ),
    );
  }
}
