import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/timer_bloc.dart';

class TimerTextView extends StatelessWidget {
  const TimerTextView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    var duration = context.select((TimerBloc value) => value.state.duration);
    final String minuteToString =
        ((duration / 60) % 60).floor().toString().padLeft(2, "0");
    final String secondToString =
        (duration % 60).floor().toString().padLeft(2, "0");

    return Center(
      child: Text(
        "$minuteToString:$secondToString",
        style: Theme.of(context).textTheme.headlineLarge,
      ),
    );
  }
}
