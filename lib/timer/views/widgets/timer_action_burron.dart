import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/timer_bloc.dart';

class TimerActionButton extends StatelessWidget {
  const TimerActionButton({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TimerBloc, TimerState>(
      builder: (context, state) {
        /// RUN
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            if (state is TimerInitial) ...[
              FloatingActionButton(
                heroTag: 'play',
                onPressed: () {
                  context
                      .read<TimerBloc>()
                      .add(TimerStarted(duration: state.duration));
                },
                child: const Icon(Icons.play_arrow),
              )
            ],

            /// PAUSE
            if (state is TimerRunInProgress) ...[
              FloatingActionButton(
                heroTag: 'pause',
                onPressed: () {
                  context.read<TimerBloc>().add(const TimerPaused());
                },
                child: const Icon(Icons.pause),
              ),
              FloatingActionButton(
                heroTag: 'reset1',
                onPressed: () {
                  context.read<TimerBloc>().add(const TimerReset());
                },
                child: const Icon(Icons.replay),
              )
            ],

            /// RESUME
            if (state is TimerRunPause) ...[
              FloatingActionButton(
                heroTag: 'resume',
                onPressed: () {
                  context.read<TimerBloc>().add(const TimerResumed());
                },
                child: const Icon(Icons.play_arrow),
              ),
              FloatingActionButton(
                heroTag: 'reset2',
                onPressed: () {
                  context.read<TimerBloc>().add(const TimerReset());
                },
                child: const Icon(Icons.replay),
              )
            ],

            /// REPLAY
            if (state is TimerRunComplete)
              FloatingActionButton(
                heroTag: 'reset2',
                onPressed: () {
                  context.read<TimerBloc>().add(const TimerReset());
                },
                child: const Icon(Icons.replay),
              )
          ],
        );
      },
    );
  }
}
