import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';
import 'package:sample_app/timer/views/timer_page.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(useMaterial3: true),
      home: const TimerScreen(),
    );
  }
}
